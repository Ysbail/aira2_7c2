# -*- coding: utf-8 -*-
# !/usr/bin/env python3
import collections 

import netCDF4 as nc
import numpy as np
import pandas as pd
import datetime as dt


def load_time(nc_variable_time):
    delta, _, *date = nc_variable_time.units.split(' ')
    head_time = pd.to_datetime('T'.join(date)).to_pydatetime()
    return np.array([head_time + dt.timedelta(**{delta: float(i)})\
                     for i in nc_variable_time[:]], dtype=dt.datetime)
                     
                     
def get_variables(paths, vars, index_header=None, skip=2, trim=20):
    out = collections.defaultdict(list)
    trim_slice = slice(None, None, skip) if trim == 0 else slice(trim, -trim, skip)
    if index_header == None:
        size = len(paths)
        heads = np.array((np.arange(size) // 4) * 4).astype(int)
    else:
        heads = np.array(index_header).astype(int)
    for i, (path, head) in enumerate(zip(paths, heads)):
        nc_file = nc.Dataset(path, 'r')
        for var in vars:
            out[var].append(np.array(nc_file.variables[var][:, trim_slice, trim_slice]).astype(
                np.float32))
    return out


def get_lats_lons(path, skip=2, trim=20):
    nc_file = nc.Dataset(path, 'r')
    trim_slice = slice(trim, -trim, skip) if trim > 0 else slice(None, None, skip)
    try:
        lats = nc_file.variables['lat'][trim_slice]
        lons = nc_file.variables['lon'][trim_slice]
    except:
        lats = nc_file.variables['latitude'][trim_slice]
        lons = nc_file.variables['longitude'][trim_slice]
    return np.array(lats).astype(np.float32), np.array(lons).astype(np.float32)


def get_lats_lons_file(nc_file, skip=2, trim=20):
    trim_slice = slice(trim, -trim, skip) if trim > 0 else slice(None, None, skip)
    try:
        lats = nc_file.variables['lat'][trim_slice]
        lons = nc_file.variables['lon'][trim_slice]
    except:
        lats = nc_file.variables['latitude'][trim_slice]
        lons = nc_file.variables['longitude'][trim_slice]
    return np.array(lats).astype(np.float32), np.array(lons).astype(np.float32)


def get_idx_pos(lat0, lon0, lats, lons):
    latvals = np.radians(lats[:])
    lonvals = np.radians(lons[:])
    lat0_rad = np.radians(lat0)
    lon0_rad = np.radians(lon0)
    minindexX = np.abs(latvals[:] - lat0_rad).argmin()
    minindexY = np.abs(lonvals[:] - lon0_rad).argmin()
    return (minindexX, minindexY)