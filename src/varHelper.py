import numpy as np

def ladder(yhat, ladder_map=[1., 5., 10., 15., 20., 25., 30.], threshold=0.5):
    out = np.zeros(yhat.shape[1:], dtype=np.uint8)
    for layer, y in enumerate(yhat): out += np.uint8((y >= threshold) & (out == layer))
    return np.r_[0., ladder_map][out]
    

def ladder_dot(yhat, ladder_map=[1., 5., 10., 15., 20., 25., 30.], threshold=0.5):
    out = np.zeros(yhat.shape[1:], dtype=np.uint8)
    for layer, y in enumerate(yhat): out += np.uint8((y >= threshold) & (out == layer))
    
    keep_left = np.where(out == 0)
    keep_right = np.where(out == len(ladder_map))
    
    base_map = np.r_[0., ladder_map][out]
    _range = np.diff(ladder_map)
    
    indexing_map = np.where(np.full((1, *yhat.shape[1:]), True))
    
    _lower = np.clip(out - 1, 0, 5)
    _upper = np.clip(out, 1, 6)
    
    _range_map = _range[_lower]
    _lower_map = yhat[(_lower.flatten(), *indexing_map[1:])].reshape(yhat.shape[1:])
    _upper_map = yhat[(_upper.flatten(), *indexing_map[1:])].reshape(yhat.shape[1:])
    
    # ((_lower_map - threshold) / (1 - threshold) + 1 - ((1 - _upper_map) - threshold) / (1 - threshold)) / 2
    # ((_lower_map - threshold) / (1 - threshold) + 1 + (- 1 + _upper_map + threshold) / (1 - threshold)) / 2
    
    # threshold = placeholder
    # ((_lower_map + _upper_map - 1) / (1 - threshold) + 1) / 2
    
    # threshold = 0.5
    # ((_lower_map - 0.5) / 0.5 + 1 + (- 1 + _upper_map + 0.5) / 0.5) / 2
    # ((_lower_map - 0.5) / 0.5 + 1 + (_upper_map - 0.5) / 0.5) / 2
    # (_lower_map - 0.5) + 0.5 + (_upper_map - 0.5)
    # _lower_map + _upper_map - 0.5
    
    _score = ((_lower_map + _upper_map - 1) / (1 - threshold) + 1) / 2
    
    _res = _score * _range_map + base_map
    _res[keep_left] = 0.
    _res[keep_right] = ladder_map[-1]
    
    return _res