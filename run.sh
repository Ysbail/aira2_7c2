#!/bin/bash

# gcloud auth activate-service-account --key-file=mycredentialsialreadyhad.json
# gcloud auth list

cd /work/home.operacao/operacao/francisco.viana/aira2_7c2_v1
source /opt/anaconda3/bin/activate py370
python main.py

# gcloud config set account equinix-ctgcp@modelagem-169213.iam.gserviceaccount.com

gsutil -m cp `date --date='1 day ago' +/storagefinep/aira2/7c2/%Y%m%d_aira2_7c2.nc` gs://gcp-equinix-ctgcp/data/aira2_as/RAW/%Y/%J/
