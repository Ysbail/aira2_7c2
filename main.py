# prepare wrf dataset in-memory?
# prepare model
# run forecast
# save forecast.dot

# from memory_profiler import profile

import gc
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import numpy as np
import netCDF4 as nc
import datetime as dt
from tensorflow.keras.models import load_model

import src.load_wrf as load_wrf
import src.ncBuilder as ncBuilder
import src.varHelper as varHelper

PATH = '/storagefapesp/data/modelos/WRF/27.0/%Y%m/M0/00/%Y%m%d00_0478S0865W0147N0194W_{_type}.nc4'
OUT_PATH = '/storagefinep/aira2/7c2/%Y%m%d_aira2_7c2.nc'
# OUT_PATH = './test.nc'
MODEL_PATH = './models/aira2_7c2_v1.h5'

# @profile
def gen_nc(dtNow):
    date_path = dtNow.strftime(PATH)
    frc_range = 24 * 16
    # input_dataset, times, lat, lon = load_wrf.get_dataset(date_path, frc_range, {})
    input_dataset = load_wrf.IterDataset(date_path, frc_range, {}, step=24)
    out_nc_file = nc.Dataset(dtNow.strftime(OUT_PATH), 'w')
    
    gc.collect()
    model = load_model(MODEL_PATH)

    times = []    
    yhat = []
    for (_input, time, lat, lon) in input_dataset:
        yhat.append(np.asanyarray(model.predict(_input))[..., 1])
        times.append(time)
        
    gc.collect()
    yhat = np.concatenate(yhat, axis=1) # (cats, time, lat, lon, bin)
    times = np.concatenate(times, axis=0)
    
    # print('check shapes:', yhat.shape, times.shape)
    
    p10 = varHelper.ladder(yhat, threshold=0.1)
    p50 = varHelper.ladder(yhat, threshold=0.5)
    p90 = varHelper.ladder(yhat, threshold=0.9)
    p95 = varHelper.ladder(yhat, threshold=0.95)
    
    dot = varHelper.ladder_dot(yhat, threshold=0.5)
    
    _vars = {'dot': {'dims': ('time', 'latitude', 'longitude'),
                     'dtype': np.float64,
                     'long_name': 'expected rain with dot algorithm',
                     'standard_name': 'expected rain',
                     'units': 'mm',
                    },
             'p10': {'dims': ('time', 'latitude', 'longitude'),
                     'dtype': np.float64,
                     'long_name': 'expected rain with low accuracy and high recall',
                     'standard_name': 'expected rain',
                     'units': 'mm',
                    },
             'p50': {'dims': ('time', 'latitude', 'longitude'),
                     'dtype': np.float64,
                     'long_name': 'expected rain with medium accuracy and medium recall',
                     'standard_name': 'expected rain',
                     'units': 'mm',
                    },
             'p90': {'dims': ('time', 'latitude', 'longitude'),
                     'dtype': np.float64,
                     'long_name': 'expected rain with high accuracy and low recall',
                     'standard_name': 'expected rain',
                     'units': 'mm',
                    },
             'p95': {'dims': ('time', 'latitude', 'longitude'),
                     'dtype': np.float64,
                     'long_name': 'expected rain with very high accuracy and very low recall',
                     'standard_name': 'expected rain',
                     'units': 'mm',
                    },
             'c0': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain of 1mm',
                    'standard_name': 'expected rain of 1mm',
                    'units': 'P(X=1mm)',
                   },
             'c1': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain of 5mm',
                    'standard_name': 'expected rain of 5mm',
                    'units': 'P(X=5mm)',
                   },
             'c2': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain of 10mm',
                    'standard_name': 'expected rain of 10mm',
                    'units': 'P(X=10mm)',
                   },
             'c3': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain of 15mm',
                    'standard_name': 'expected rain of 15mm',
                    'units': 'P(X=15mm)',
                   },
             'c4': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain of 20mm',
                    'standard_name': 'expected rain of 20mm',
                    'units': 'P(X=20mm)',
                   },
             'c5': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain of 25mm',
                    'standard_name': 'expected rain of 25mm',
                    'units': 'P(X=25mm)',
                   },
             'c6': {'dims': ('time', 'latitude', 'longitude'),
                    'dtype': np.float64,
                    'long_name': 'expected rain of 30mm',
                    'standard_name': 'expected rain of 30mm',
                    'units': 'P(X=30mm)',
                   },
            }
    
    ncBuilder.createNC(out_nc_file, dtNow, lat, lon, time=times, vars=_vars)
    ncBuilder.update_NC(out_nc_file, 'dot', dot, )
    ncBuilder.update_NC(out_nc_file, 'p10', p10, )
    ncBuilder.update_NC(out_nc_file, 'p50', p50, )
    ncBuilder.update_NC(out_nc_file, 'p90', p90, )
    ncBuilder.update_NC(out_nc_file, 'p95', p95, )
    ncBuilder.update_NC(out_nc_file, 'c0', yhat[0], )
    ncBuilder.update_NC(out_nc_file, 'c1', yhat[1], )
    ncBuilder.update_NC(out_nc_file, 'c2', yhat[2], )
    ncBuilder.update_NC(out_nc_file, 'c3', yhat[3], )
    ncBuilder.update_NC(out_nc_file, 'c4', yhat[4], )
    ncBuilder.update_NC(out_nc_file, 'c5', yhat[5], )
    ncBuilder.update_NC(out_nc_file, 'c6', yhat[6], )
    
    out_nc_file.close()


if __name__ == '__main__':
    dtNow = dt.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0)
    gen_nc(dtNow)
    # for dtNow in [dtNow + dt.timedelta(days=-i) for i in range(14)][::-1]: gen_nc(dtNow)
